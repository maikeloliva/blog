package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.User;

@Repository
@Transactional
public class UserDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	/*
	 * Almacena el usuario en la base de datos
	 */
	public void create(User usuario) {
		entityManager.persist(usuario);
		return;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAll(){
		return entityManager.createQuery("select u from User u")
				.getResultList();
		
	}
}
