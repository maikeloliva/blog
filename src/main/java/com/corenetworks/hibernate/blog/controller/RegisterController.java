package com.corenetworks.hibernate.blog.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.RegisterBean;
import com.corenetworks.hibernate.blog.dao.UserDao;
import com.corenetworks.hibernate.blog.model.User;



@Controller
public class RegisterController {

	@Autowired
	private UserDao usuarioDao;
	
	@GetMapping(value="/signup")
	public String mostrarFormulario(Model modelo) {
		modelo.addAttribute("userRegister", new RegisterBean());
		return "register";
	}
	@PostMapping(value="/register")
	public String submit(@ModelAttribute("userRegister") RegisterBean r, Model modelo ) {
		usuarioDao.create(new User(
				r.getNombre(), r.getCiudad(),  r.getEmail(), r.getPassword()
				));
		return "redirect:/autores";
	}
	
	
	
}
